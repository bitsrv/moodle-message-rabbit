<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'message_rabbit', language 'en'
 *
 * @package    message_rabbit
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configrabbithost'] = 'Rabbit服务器所在IP地址';
$string['configrabbitvhost'] = 'Rabbit服务器所在虚拟机名称，可为空';
$string['configrabbitusername'] = 'Rabbit系统登录用户名';
$string['configrabbitpassword'] = 'Rabbit系统登录密码';
$string['configrabbitport'] = 'Rabbit服务网络端口';
$string['configrabbitexchange'] = '收发协议中确定的exchange名称';
$string['configrabbitdeliverymode'] = '消息传输模式，默认为direct';
$string['configrabbitdurable'] = '消息是否持久，0为非持久，1为持久';
$string['rabbithost'] = 'rabbit host';
$string['rabbitid'] = 'rabbit ID';
$string['rabbitvhost'] = 'rabbit vhost';
$string['rabbitusername'] = 'rabbit user name';
$string['rabbitpassword'] = 'rabbit password';
$string['rabbitport'] = 'rabbit port';
$string['rabbitexchange'] = 'exchange name';
$string['rabbitdeliverymode'] = 'exchange mode';
$string['rabbitdurable'] = 'durable';
$string['notconfigured'] = 'Rabbit服务未配置，无法发送消息';
$string['pluginname'] = 'rabbit message';
